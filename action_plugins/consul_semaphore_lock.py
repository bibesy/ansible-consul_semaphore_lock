#!/bin/env python3

import os
import consul
import json
from threading import Thread
from time import sleep
from ansible.plugins.action import ActionBase
from ansible.errors import AnsibleActionFail

class ConsulSessionKeepAlive(object):
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, connection, session_id, duration, consul_token, consul_dc):
        while self._running:
            connection.session.renew(session_id, token=consul_token, dc=consul_dc)
            sleep(duration)

class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        new_task = self._task.copy()
        next_module = new_task.args.pop('module', None)
        if not next_module:
            raise AnsibleActionFail('No module specified')
        wrapper_vars = new_task.args.pop('wrapper_vars',{})
        consul_server = wrapper_vars.get('consul_server', '127.0.0.1')
        consul_port = wrapper_vars.get('consul_port', 8500)
        consul_token = wrapper_vars.get('consul_token', None)
        consul_scheme = wrapper_vars.get('consul_scheme', 'http')
        consul_dc = wrapper_vars.get('consul_dc', None)
        consul_verify = wrapper_vars.get('consul_verify', True)
        consul_cert = wrapper_vars.get('consul_cert', None)
        consul_session_name = wrapper_vars.get('consul_cert', 'ansible consul_semaphore_lock')
        lock_prefix = wrapper_vars.get('lock_prefix', 'ansible/locks')
        lock_key = wrapper_vars.get('lock_key', next_module)
        limit = wrapper_vars.get('lock_limit', 1)
        session_ttl = wrapper_vars.get('session_ttl', 10)
        keepalive_interval = wrapper_vars.get('session_ttl', 3)
        prefix=f'{lock_prefix}/{lock_key}'
        lock=f'{prefix}/.lock'

        c = consul.Consul(host=consul_server, port=consul_port, token=consul_token, scheme=consul_scheme, dc=consul_dc, verify=consul_verify, cert=consul_cert)
        session_id = c.session.create(ttl=session_ttl, name=consul_session_name, behavior="delete", token=consul_token, dc=consul_dc)
        keepalive = ConsulSessionKeepAlive()
        thread = Thread(target = keepalive.run, args = (c, session_id, keepalive_interval, consul_token, consul_dc), daemon=True)
        thread.start()

        rv = c.kv.put(f'{prefix}/{session_id}', None, acquire=session_id, token=consul_token, dc=consul_dc)
        if rv == False:
            raise AnsibleActionFail('Failed to create contender entry')
        
        lock_body = {
            'Limit': limit,
            'Holders': [session_id]
        }
        c.kv.put(lock, json.dumps(lock_body), cas=0, token=consul_token, dc=consul_dc) 
        
        index = None
        while True:
            index, data = c.kv.get(prefix, recurse=True, index=index, token=consul_token, dc=consul_dc)
            data = sorted(data, key=lambda x: x['CreateIndex'])
            if data is not None:
                try:
                    lock_data = [x for x in data if x['Key'] == lock][0]
                except:
                    pass
            else:
                raise AnsibleActionFail(f'{lock} disappeared')
            sessions = []
            for i in data:
                if i['Key'] != lock:
                    sessions.append(os.path.basename(i['Key']))
            sessions_set = set(sessions)
            lock_values = json.loads(lock_data.get('Value','{}'))
            holders = set()
            if lock_values:
                holders = set(lock_values.get('Holders'))
                read_limit = lock_values.get('Limit')
            missing_sessions = holders.difference(sessions_set)
            remaining_holders = holders.difference(missing_sessions)
        
            if session_id in missing_sessions:
                raise AnsibleActionFail('My session was disconnected')
        
            queue_length = len(sessions) 
            try:
                position_in_queue = sessions.index(session_id) + 1
            except:
                raise AnsibleActionFail('Ejected from queue')
        
            if position_in_queue > limit:
                print(f'In queue {position_in_queue} of {queue_length} [{read_limit} active]')
        
            if len(missing_sessions) > 0:
                lock_body = {
                    'Limit': read_limit,
                    'Holders': list(remaining_holders) 
                }
                lock_modify_index = lock_data.get('ModifyIndex', 0)
                c.kv.put(lock, json.dumps(lock_body), cas=lock_modify_index, token=consul_token, dc=consul_dc) 
            elif len(holders) < read_limit:
                if position_in_queue != len(remaining_holders) + 1:
                    # if we are not next in the queue give the next one
                    # a couple seconds to proceed otherwise we will attempt to grab the spot
                    #wait_time = 1 + queue_length - position_in_queue 
                    wait_time = min(5, max(1, (position_in_queue - limit)))
                    sleep(wait_time)
        
                lock_body = {
                    'Limit': limit,
                    'Holders': list(remaining_holders) + [session_id]
                }
                lock_modify_index = lock_data.get('ModifyIndex',0)
                rv = c.kv.put(lock, json.dumps(lock_body), cas=lock_modify_index, token=consul_token, dc=consul_dc) 
                if rv == True:
                    print(f'Starting module {next_module}')
                    module_return = self._execute_module(module_name=next_module,
                                                 module_args=new_task.args,
                                                 task_vars=task_vars, tmp=tmp)
                    keepalive.terminate()
                    c.session.destroy(session_id)
                    return module_return
